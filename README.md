# Playlist Share Docker

## Overview

Playlist Share is a web application to publish your list of listenings.

![Screenshot of Playlist Share](screenshot.png)

You can add an album, mark it as private, as listened, rate it and add a comment to it.

More information in backend and frontend README.

This project is still ongoing development.

This repository contains the necessary files and configurations to build and deploy the Docker container for Playlist Share.

Note that this project is still ongoing development, even if you can use it in production environment, this come with no guarantees.

## Table of Contents

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Configuration](#configuration)
- [Contributing](#contributing)
- [License](#license)

## Getting Started

### Prerequisites

Before you begin, ensure that you have the following installed:

- Docker: [https://www.docker.com/get-started](https://www.docker.com/get-started)

### Installation

If you want to include the webserver into the containers, you have to uncomment nginx service in the `docker-compose.yml`.

1. Clone this repository with `git clone https://framagit.org/playlistshare/playlistshare-docker.git`
1. Init gitsubmodules with `git submodule update --init --recursive`
1. Update frontend and backend with `git submodule update --recursive --remote`
1. Change environment variable in backend and frontend Dockerfile (see [Configuration](#configuration))
1. (Optional) Update nginx.conf with your server URL (see [Configuration](#configuration))

When all is done, you can run `docker compose up --build`, that build and run the container.

## Configuration

Below is the reference for frontend and backend environment variables.

* Frontend environment variables
    * `REACT_APP_API_URL`: base url for backend API, by default, it's "http://localhost/backend/api/v1"
    * `REACT_APP_DEBUG`: enable or disable debug mode (which is not really implemented right now), 0 by default
    * `REACT_APP_TITLE`: Title of the application ("Playlist Share" by default)
    * `REACT_APP_MAX_ITEM_PER_PAGE`: Number of item by page, 10 by default
* Backend environment variables
    * `APP_URL`: http://localhost/backend
    * `FRONTEND_URL`: http://localhost
    * `ADMIN_NAME`: Name of admin user, you have to change this
    * `ADMIN_EMAIL`: Email of admin user, you have to change this
    * `ADMIN_PASSWORD`: Password of admin user, you have to change this

The `nginx.conf` must be change for you url

    events {}
    http {
        server {
            listen 80;
            server_name  <your_url>;

            location / {
                proxy_pass http://frontend:3000;
            }

            location /backend {
                proxy_pass http://backend:8000;
                rewrite ^/backend(.*)$ $1 break;
            }
        }
    }

## Contributing

Thank you for your interest in contributing to Playlist Share! At the moment, we are not accepting direct contributions to this repository. However, you are welcome to fork the repository and work on any enhancements or modifications independently.

[See all repositories for PlaylistShare](https://framagit.org/playlistshare/)

## License

This work is licensed under Mozilla Public License Version 2.0, see the [LICENSE](LICENSE) file for details.
